CREATE TABLE users
(
    id         serial PRIMARY KEY,
    username   VARCHAR(50) UNIQUE NOT NULL,
    email      VARCHAR(255)       NOT NULL,
    created_at DATE               NOT NULL
);

CREATE TABLE artists
(
    id          serial PRIMARY KEY,
    user_id     bigint unsigned UNIQUE,
    artist_name VARCHAR(100),
    tagline     VARCHAR(255) NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE tracks
(
    id        serial PRIMARY KEY,
    artist_id bigint unsigned NOT NULL,
    name      VARCHAR(255) NOT NULL,
    isrc      VARCHAR(12),
    FOREIGN KEY (artist_id) REFERENCES artists (id)
);

INSERT INTO users(id, username, email, created_at)
VALUES (1, 'user1', 'user1@company.com', '2021-02-02'),
       (2, 'user2', 'user2@company.com', '2022-03-02'),
       (3, 'user3', 'user3@company.com', '2021-08-05');

INSERT INTO artists(id, user_id, artist_name, tagline)
VALUES (1, 1, 'artist1', 'tag'),
       (2, 2, 'artist2', 'tag2'),
       (3, null, 'artist3', 'tag3');

INSERT INTO tracks(artist_id, name, isrc)
VALUES (1, 'Track 1', 'QQHHGG'),
       (1, 'Track 2', 'QQHHVV'),
       (1, 'Track 3', 'AAHHGG');

SELECT u.id   AS user_id,
       u.username,
       u.email,
       a.id   AS artist_id,
       a.tagline,
       t.name AS track_name,
       t.isrc AS track_isrc
FROM artists AS a
         LEFT JOIN users AS u on u.id = a.user_id
         LEFT JOIN tracks AS t on a.id = t.artist_id
WHERE u.id IS NOT NULL;
