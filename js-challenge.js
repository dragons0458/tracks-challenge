const response = [
    {
        id: 1293487,
        name: 'KCRW',  // radio station callsign
        tracks: [{timestp: '2021-04-08', trackName: 'Peaches'}]
    },
    {
        id: 12923,
        name: 'KQED',
        tracks: [
            {timestp: '2021-04-09', trackName: 'Savage'},
            {timestp: '2021-04-09', trackName: 'Savage (feat. Beyonce)'},
            {timestp: '2021-04-08', trackName: 'Savage'},
            {timestp: '2021-04-08', trackName: 'Savage'},
            {timestp: '2021-04-08', trackName: 'Savage'}
        ]
    },
    {
        id: 4,
        name: 'WNYC',
        tracks: [
            {timestp: '2021-04-09', trackName: 'Captain Hook'},
            {timestp: '2021-04-08', trackName: 'Captain Hook'},
            {timestp: '2021-04-07', trackName: 'Captain Hook'}
        ]
    }
];

/**
 * Allows to get a map of tracks indexed by timestamp.
 * @param response The data to be indexed.
 * @return {Map<string, Map<string, number>>} The map of tracks indexed by timestamp.
 */
function indexData(response) {
    const indexMap = new Map();

    for (const res of response) {
        for (const track of res.tracks) {
            const key = track.timestp;

            if (!indexMap.has(key)) {
                indexMap.set(key, new Map());
            }

            const trackCountMap = indexMap.get(key);
            const value = trackCountMap.get(track.trackName) || 0;
            trackCountMap.set(track.trackName, value + 1);
        }
    }

    return indexMap;
}

/**
 * Process the data and return info like graph requirement.
 * @param response The data to be processed.
 * @return {{x: string, y: number, tooltip: string}[]} The graph data.
 */
function processIndexData(response) {
    const indexedData = indexData(response);
    const data = [];

    for (const [x, trackCountMap] of indexedData) {
        let y = 0;
        let tooltip = '';

        for (const [trackName, count] of trackCountMap) {
            tooltip += `${trackName} (${count}), `;
            y += count;
        }

        data.push({
            x,
            y,
            tooltip: tooltip.slice(0, -2)
        });
    }

    return data;
}

console.log(processIndexData(response));

module.exports = {
    indexData,
    processIndexData,
    response
};
