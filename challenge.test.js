const assert = require('assert');
const {indexData, processIndexData, response} = require('./js-challenge');

const date = '2021-04-07';

const indexedMap = indexData(response);

assert.strictEqual(indexedMap.size, 3, 'Map size should be 3');
assert.strictEqual(indexedMap.has(date), true, `Map should contain key ${date}`);
assert.strictEqual(indexedMap.get(date).size, 1, `Map should contain 1 item for ${date}`);

const processedData = processIndexData(response);

assert.strictEqual(processedData.length, 3, 'Array length should be 3');

const graphPoint = processedData.find(point => point.x === '2021-04-08');

assert.strictEqual(graphPoint.y, 5, 'Graph point y should be 5');

console.log('All tests passed');
